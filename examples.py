#
# Example 1: minimal
#
utils = utilities()
path = "my_path"  # e.g. "C:/Users/me/Documents/"
utils.write_text_file(path, "tempX.txt", "hello world")



#
# Example 2: build dha files
#
utils = utilities()
dha = dha()

path = "my_path"  # e.g. "C:/Users/me/Documents/"

utils.write_last_updated_time(path, "lastupdated.js", "last updated: ")  # last updated time to DHA javaScript function

data = utils.read_gpkg(path, "my_geodata.gpkg")  # read geo data

utils.write_geojson(path, "geoJsonFile1.geojson", data)  # geo data to geojson
utils.write_geojson(path, "geoJsonFile2.geojson", data)  # geo data to geojson

layer1 = dha.build_single_layer("geoJsonFile1.geojson", "friendlyName1","radioButtonValue1",
                                "['layerNameT1','layerNameT2']","geojsonName","geojsonGeom")  # first map layer with 2 time points
layer2 = dha.build_single_layer("geoJsonFile2.geojson", "friendlyName2","radioButtonValue2", 
                                "['layerNameTA','layerNameTB','layerNameTC']","geojsonName","geojsonGeom")  # second map layer with 3 time points 

config_data = dha.event_listener("friendlyName1")           # start config data with javaScript event listener
config_data += dha.global_vars(radioNotDropdown="false", InitialMapCenterLatLng="[54.3, -3.5]", 
                               InitialmapZoom="5", mapUnits="false", secondMap="true", 
                               userDefinedBaseMaps="true")  # add javaScript global variables
config_data += dha.build_list_of_layers([layer1, layer2])   # add javaScript config info for each group of layers

utils.write_text_file(path, "allMetadata.js", config_data)
