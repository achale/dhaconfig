"""Dynamic Health Atlas Configuration file utility functions"""

from pathlib import Path

import geopandas as gpd

from datetime import datetime
from geojson import dump


class utilities:
    def write_text_file(self, floc, fname, content):
        """Save text file overwriting any existing content
        :param: floc: (str) target file location
        :param: fname: (str) file name
        :param: content: (str) file name not including extension
        :return: None
        """
        f = open(Path(floc) / fname, "w")  # full location of file
        f.write(content)
        f.close()

    def write_last_updated_time(self, floc, fname, msg):
        """Save javaScript file containing date-time this function is run
        :param: floc: (str) target file location
        :param: fname: (str) file name
        :param: msg: (str) text string before time
        :return: None
        """
        now = datetime.now().astimezone()
        str = now.strftime("%H:%M %d %b %Y (%z)")  # %Z timezone name
        content = (
            "function lastupdated(divName){document.getElementById(divName).innerHTML='"
            + msg
            + str
            + "'}"
        )
        self.write_text_file(floc, fname, content)

    def write_geojson(self, floc, fname, data):
        """Save geojson file overwriting any existing content
        :param: floc: (str) target file location
        :param: fname: (str) file name
        :param: data: spatial data
        :return: None
        """
        with open(Path(floc) / fname, "w") as f:
            dump(data, f, separators=(",", ":"))  # separators removes white space etc

    def read_gpkg(self, floc, fname):
        """Open gpkg file
        :param: floc: (str) target file location
        :param: fname: (str) file name
        :return: GIS data - minimally this should contain the GIS geometries of points or polygons
        """
        try:
            data = gpd.read_file(Path(floc) / fname)
        except IOError:
            raise
        return data


class dha:
    def event_listener(self, defaultMap, postfix="", startdate7="", startdate14=""):
        """Set javaScript event listener to load default and user defined layers
        :param: defaultMap: (str) name of map layer which should be loaded by default when DHA is loaded
        :param: postfix: (str) postfix to name of map layer when layer is loaded from url. This is useful if all 'first' layers end with the same string e.g. the same date.
        :param: startdate7: (str) additional map layer name specifically for covid19
        :param: startdate14: (str) additional map layer name specifically for covid19
        :return: (str) javaScript event listener for DHA defining which layer should initially load into the browser
        :Example:
            Loading the DHA with "https://domain.name/" results in the default layer being loaded as defined by the parameter 'defaultMap'.
            Loading the DHA with "https://domain.name/index.html?layer=layer name" loads a layer called 'layer name' + ' ' + postfix.
        """
        startup = (
            "addEventListener('load',\n"
            "function(){\n"
            "setTimeout(function(){\n"
            "var ln,\n"
            "    layerIDplusDate,\n"
            "    layIDplusFromDate7,\n"
            "    layIDplusFromDate14;\n"
            "document.getElementById('loadholderbacking').style.display='none';\n"
            "if (typeof layerId==='undefined') {\n"
            "  chooseLayer('" + defaultMap + "');\n"
            "} else {\n"
            "  layerIDplusDate = layerId + '" + postfix + "';\n"
            "  layIDplusFromDate7 = layerId + '" + startdate7 + "';\n"
            "  layIDplusFromDate14 = layerId + '" + startdate14 + "';\n"
            "  for ( var i=0; i<geodata.length; i++ ) {\n"
            "    ln = geodata[i].options.layerName;\n"
            "    if ( layerId===ln || layerIDplusDate===ln || layIDplusFromDate7===ln || layIDplusFromDate14===ln ) {\n"
            "      map.addLayer(geodata[i]);\n"
            "      break;\n"
            "    }\n"
            "  }\n"
            "}\n"
            "}, 2000)\n"
            "});\n"
        )
        return startup

    def global_vars(
        self,
        radioNotDropdown="false",
        InitialMapCenterLatLng="[54.3, -3.5]",
        InitialmapZoom="5",
        mapUnits="false",
        secondMap="true",
        userDefinedBaseMaps="true",
    ):
        """Set up global vars for DHA
        :param: radioNotDropdown: (str) javaScript boolean. When true a list of radio buttons instead of a dropdown menu are used to select groups of layers. A 'group of layers' refers to all the layers relating to a specific disease for a given number of timesteps.
        :param: InitialMapCenterLatLng: (str) javaScript list, e.g. [54.3,-3.5]. Initial centre of the baselayer map (e.g. OpenStreetMap) defined in an array of the form [latitude, longitude].
        :param: InitialmapZoom: (str) number defining initial zoom level of baselayer map, e.g. "5".
        :param: mapUnits: (str) javaScript boolean. When true the baselayer map displays its scale in both miles and kilometres otherwise when false only miles are shown.
        :param: secondMap: (str) javaScript boolean. There are 2 base map layers are defined with different properties e.g. the defaults are OpenStreetMap with opacity:0.6 and opacity:1.0. If secondMap=true when the web page initially loads the second is selected (opacity:1.0) otherwise the first is selected (opacity:0.6).
        :param: userDefinedBaseMaps: (str) javaScript boolean. The default baselayer is a OpenStreetMap (userDefinedBaseMaps=true). If using Google Maps (userDefinedBaseMaps=false) then a Google account KEY should be placed in either index.html. In the case where MutationObserver() is not supported by the web browser the fallback is OpenStreetMap.
        :return: (str) user defined global variables for DHA, formatted for javaScript
        """
        vars = (
            "var radioNotDropdown = " + radioNotDropdown + ";\n"
            "var InitialMapCenterLatLng = " + str(InitialMapCenterLatLng) + ";\n"
            "var InitialmapZoom = " + InitialmapZoom + ";\n"
            "var mapUnits = " + mapUnits + ";\n"
            "var secondMap = " + secondMap + ";\n"
            "var userDefinedBaseMaps = " + userDefinedBaseMaps + ";\n"
        )
        return vars

    def build_list_of_layers(self, list_of_layers):
        """ build full list of layers for DHA
        :param: list_of_layers: (list) [layer1, layer2, ...] python list of layers generated by build_single_layer(), note that each layer is a javaScript dictionary
        :return: (str) list of layers formatted for javaScript, this is a list of dictionaries i.e. "[{layer1},{layer2},{...}, ...]"
        """
        list = "var layerdata = [\n"
        i = 1
        for x in list_of_layers:
            list += x
            list += ",\n" if i < (len(list_of_layers)) else ""
            i += 1
        list += "\n];\n"
        return list

    def mapPosition(self, centerLatLng="[54.3,-3.5]", zoom="5"):
        """
        :param: centerLatLng: (str) javaScript array, e.g. "[54.3,-3.5]". Initial centre of the baselayer map (e.g. OpenStreetMap) defined in an array of the form [latitude, longitude]
        :param: zoom: (str) number defining initial zoom level of baselayer map, e.g. "3"
        :return: (str) javaScript dictionary
        """
        txt = "{" "centerLatLng:" + str(centerLatLng) + ", " "zoom:" + zoom + "}"
        return txt

    def regionNames(self, country="undefined", area="undefined"):
        """
        :param: country: (str) javaScript string defining collective name of all features, e.g. "England", or the string can give general instruction e.g. "Click region in UK to view its graph"
        :param: area: (str) javaScript string defining overall name of the map features e.g. "local districts", "hotels", ...
        :return: (str) javaScript dictionary
        """
        if country == "undefined":
            country = ""
        if area == "undefined":
            area = ""
        txt = (
            "{" "country:" + self.toStr(country) + ", " "area:" + self.toStr(area) + "}"
        )
        return txt

    def colgrades(
        self,
        colours="['#5e4fa2','#d7191c']",
        legtitle="undefined",
        Ints="5",
        Inis="0",
        Num="undefined",
        userDefined="undefined",
    ):
        """
        :param: legtitle: (str) DHA map legend title e.g. "Prevalence"
        :param: Ints: (str) number defining size of increments on  DHA map legend e.g. "2.5"
        :param: Inis: (str) number defining initial value on legend e.g. "1.5"
        :param: Num: (str) if Ints is "undefined" then this defines the number of legend incrememts e.g. "7", otherwise set to "undefined"
        :param: colours: (str) javaScript list of strings defining hexadecimal colours e.g. "['#5e4fa2','#66c2a5','#ffffbf','#fdae61','#d7191c']"
        :param: userDefined: if Ints and Inis are both "undefined" this can be used to define the legend incrememts "[0,0.03,0.05,0.95,0.97,1]"
        :return: (str) javaScript dictionary
        """
        txt = (
            "{"
            "legtitle:" + self.toStr(legtitle) + ", "
            "Ints:" + Ints + ", "
            "Inis:" + Inis + ", "
            "Num:" + Num + ", "
            "Vals:" + str(colours) + ", "
            "userDefined:" + str(userDefined) + "}"
        )
        return txt

    def mapStyles(
        self,
        weight="1",
        opacity="undefined",
        color="#000",
        fillOpacity="0.8",
        smoothFactor="1",
        radius="6",
    ):
        """
        :param: weight: (str) defines the weight of the border surrounding each map feature e.g. "2"
        :param: opacity: (str) defines the opacity of the border surrounding each map feature, value range from 0 to 1, e.g. "0.8"
        :param: color: (str) defines the colour of the border surrounding each map feature using hexadecimal coding e.g. "#0000FF"
        :param: fillOpacity: (str) defines the opacity of the fill colour for each map feature, value range from 0 to 1, e.g. "0.8"
        :param: smoothFactor: (str) in relation to the map this defines a number which allows polyline simplification to improve performance times; the default is 1, higher numbers improve performance
        :param: radius: (str) if the feature is a point this defines the radius of the point on the map e.g. "10"
        :return: (str) javaScript dictionary
        """
        txt = (
            "{"
            "weight:" + weight + ", "
            "opacity:" + opacity + ", "
            "color:" + self.toStr(color) + ", "
            "fillOpacity:" + fillOpacity + ", "
            "smoothFactor:" + smoothFactor + ", "
            "radius:" + radius + "}"
        )
        return txt

    def featurehltStyle(self, weight="3", color="#F00"):
        """
        :param: weight: (str) defines the weight of the border surrounding each map feature e.g. "3" when it is selected on the map
        :param: color: (str) defines the colour of the border surrounding each map feature using hexadecimal coding e.g. "#0000FF"
        :return: (str) javaScript dictionary
        """
        txt = "{" "weight:" + weight + ", " "color:" + self.toStr(color) + "}"
        return txt

    def timeData(
        self,
        xlabs="undefined",
        timeseries="true",
        CIname="95% CI",
        highlight="undefined",
        timeseriesMin="0",
        timeseriesMax="0",
        timeseriesStep="1",
    ):
        """
        :param: xlabs: (str) javaScript list defining x-axis tick labels on timeseries e.g. "['29 May', '05 Jun', '12 Jun', '19 Jun', '26 Jun', '03 Jul', '10 Jul', '17 Jul', '24 Jul']"
        :param: timeseries: (str) javaScript boolean, if "true" a timeseries will be displayed and xlabs must be defined
        :param: CIname: (str) name of confidence intervals, if they exist
        :param: highlight: (str) this is for an array defining the start ('xLeft') and finish ('xRight') of a band of background colour ('colorFill') along the timeseries x-axis (height of band is full length of y-axis). The 'title' parameter can be used to describe the band. If 'xLeft' is set to undefined the band will start at the lowest x-axis value. If 'xRight' is set to undefined the band will finish at the highest x-axis value. If a highlighted band is not required set 'highlight' to undefined.
        :param: timeseriesMin: (str) timeseries starting value as defined in geojsonname
        :param: timeseriesMax: (str) timeseries ending value as defined in geojsonname
        :param: timeseriesStep: (str) timeseries step size as defined in geojsonname
        :return: (str) javaScript dictionary
        """
        # str(len(list(map(str.strip, xlabs.strip('][').replace("'", "").split(',')))))
        if CIname == "undefined":
            CIname = ""
        if timeseries == "false":
            txt = "{timeseries:false}"
        else:
            txt = (
                "{"
                "timeseries:true, "
                "xlabs:" + str(xlabs) + ", "
                "CIname:" + self.toStr(CIname) + ", "
                "highlight:" + highlight + ", "
                "timeseriesMin:" + timeseriesMin + ", "
                "timeseriesMax:" + timeseriesMax + ", "
                "timeseriesStep:" + timeseriesStep + "}"
            )
        return txt

    def timePlot(
        self,
        Background1Colour="#FFF",
        Line1Colour="#FFF",
        Background2Colour="#007ac3",
        Line2Colour="#007ac3",
        Background3Colour="undefined",
        Line3Colour="undefined",
        MarkerSize="3",
        HighlightColour="#b41019",
        HighlightSize="5",
        ymax="undefined",
        beginYAtZero="true",
    ):
        """
        :param: Background1Colour: (str) defines the background colour of the 'meandata' (format hexadecimal or rgba)
        :param: Line1Colour: (str) defines  the line colour of the 'meandata'(format hexadecimal or rgba)
        :param: Background2Colour: (str) defines the background colour of the feature data (format hexadecimal or rgba)
        :param: Line2Colour: (str) defines the line colour of the feature data (format hexadecimal or rgba)
        :param: Background3Colour: (str) defines the background colour of the upper and lower confidence interval data (format hexadecimal or rgba)
        :param: Line3Colour: (str) defines the line colour of the upper and lower confidence intervals data (format hexadecimal or rgba)
        :param: MarkerSize: (str) defines the size of the markers on the timeseries e.g. "3"
        :param: HighlightColour: (str) defines the line colour of the highlighted point and vertical line on timeseries (format hexadecimal or rgba)
        :param: HighlightSize: (str) defines the marker size of the highlighted points on the timeseries plot e.g. "3"
        :param: ymax: (str) defines the maximum y-axis value on the timeseries e.g. "10.5": note that if ymax="undefined" then the maximum y-axis value is computed dynamically
        :param: beginYAtZero: (str) javaScript boolean, when "true" the timeseries y-axis starts at zero otherwise it is computed dynamically
        :return: (str) javaScript dictionary
        """
        txt = (
            "{"
            "Background1Colour:" + self.toStr(Background1Colour) + ", "
            "Line1Colour:" + self.toStr(Line1Colour) + ", "
            "Background2Colour:" + self.toStr(Background2Colour) + ", "
            "Line2Colour:" + self.toStr(Line2Colour) + ", "
            "Background3Colour:" + self.toStr(Background3Colour) + ", "
            "Line3Colour:" + self.toStr(Line3Colour) + ", "
            "MarkerSize:" + MarkerSize + ", "
            "HighlightColour:" + self.toStr(HighlightColour) + ", "
            "HighlightSize:" + HighlightSize + ", "
            "ymax:" + ymax + ", "
            "beginYAtZero:" + beginYAtZero + "}"
        )
        return txt

    def units(self, html="undefined", unicode="undefined", xlab="undefined"):
        """
        :param: html: (str) javaScript string defining y-axis label for timeseries using html characters e.g. "cases / 10 <span style='vertical-align:super'>3</span> pop"
        :param: unicode: (str) javaScript string defining y-axis label for timeseries using unicode characters e.g. "cases / 10\u00B3 pop"
        :param: xlab: (str) javaScript string defining x-axis label for timeseries using html coding e.g. "week numbers in 2020"
        :return: (str) javaScript dictionary
        """
        if html == "undefined":
            html = ""
        if unicode == "undefined":
            unicode = ""
        if xlab == "undefined":
            xlab = ""
        txt = (
            "{"
            "html:" + self.toStr(html) + ", "
            "unicode:" + self.toStr(unicode) + ", "
            "xlab:" + self.toStr(xlab) + "}"
        )
        return txt

    def toStr(self, x):
        """
        :param: x: (str) a string which may or may not be "undefined"
        :return: (str) string inside quotes
        """
        y = x if x == "undefined" else "'" + x + "'"
        return y

    def build_single_layer(
        self,
        geoJsonFile,
        friendlyName,
        radioButtonValue,
        layerName,
        geojsonName,
        geojsonGeom,
        geojsonExtraInfo="undefined",
        mapPosition_centerLatLng="[54.3,-3.5]",
        mapPosition_zoom="5",
        regionNames_country="undefined",
        regionNames_area="undefined",
        colgrades_colours="['#5e4fa2','#d7191c']",
        colgrades_legtitle="undefined",
        colgrades_Ints="5",
        colgrades_Inis="0",
        colgrades_Num="undefined",
        colgrades_userDefined="undefined",
        legend="true",
        sliderlabel="undefined",
        mapStyles_weight="1",
        mapStyles_opacity="undefined",
        mapStyles_color="#000",
        mapStyles_fillOpacity="0.8",
        mapStyles_smoothFactor="1",
        mapStyles_radius="6",
        noDataColour="rgba(0, 0, 0, 0.3)",
        featurehltStyle_weight="3",
        featurehltStyle_color="#F00",
        timeData_xlabs="undefined",
        timeData_timeseries="true",
        timeData_CIname="95% CI",
        timeData_highlight="undefined",
        timeData_timeseriesMin="0",
        timeData_timeseriesMax="0",
        timeData_timeseriesStep="1",
        meandata="undefined",
        timePlot_Background1Colour="#FFF",
        timePlot_Line1Colour="#FFF",
        timePlot_Background2Colour="#007ac3",
        timePlot_Line2Colour="#007ac3",
        timePlot_Background3Colour="undefined",
        timePlot_Line3Colour="undefined",
        timePlot_MarkerSize="3",
        timePlot_HighlightColour="#b41019",
        timePlot_HighlightSize="5",
        timePlot_ymax="undefined",
        timePlot_beginYAtZero="true",
        layerMarker="undefined",
        mapBoundary="undefined",
        units_html="undefined",
        units_unicode="undefined",
        units_xlab="undefined",
        downloadData="undefined",
        popupBox="false",
    ):
        """ build a single layer of dictionary values for DHA
        :param: geoJsonFile: (str) javaScript string defining path and file name of geoJson file relative to the website root e.g. "data/filename.geojson"
        :param: friendlyName: (str) javaScript string definingoverall human-readable name assigned to the layers e.g. "Prevalence per 100k", this will be displayed on DHA's dropdown/radio list
        :param: radioButtonValue: (str) defines which layer is loaded by default when DHA website loads, it should have the same name as one of the layers e.g. "Prevalence per 100k 29 May"
        :param: geojsonName: (str) javaScript list of strings defining names of the layers as defined in geoJsonFile, structured as follows "['0','1','2','...']"
        :param: layerName: (str) javaScript list of strings human-readable text name assigned to each layer, structured as follows "['layer0','layer1','layer2','...']"
        :param: geojsonGeom: (str) javaScript string defining name of geometry, for points or polygons, as defined in geoJsonFile e.g. "local district"
        :param: geojsonExtraInfo: (str) javaScript string defining additional field associated with the geoJson geometry as defined in geoJsonFile e.g. "local district code"
        :param: mapPosition_centerLatLng: (str) javaScript array, e.g. [54.3,-3.5]. Initial centre of the baselayer map (e.g. OpenStreetMap) defined in an array of the form [latitude, longitude]
        :param: mapPosition_zoom: (str) javaScript string defining initial zoom level of baselayer map expressed as an integer
        :param: regionNames_country: (str) javaScript array giving collective name of all features, e.g. "England", or the string can give general instruction e.g. "Click region in UK to view its graph"
        :param: regionNames_area: (str) javaScript string giving overall name of the map features e.g. "local districts", "hotels", ...
        :param: colgrades_colours: (str) javaScript list of strings defining hexadecimal colours e.g. "['#5e4fa2','#66c2a5','#ffffbf','#fdae61','#d7191c']"
        :param: colgrades_legtitle: (str) DHA map legend title e.g. "Prevalence"
        :param: colgrades_Ints: (str) number giving size of increments on  DHA map legend e.g. "2.5"
        :param: colgrades_Inis: (str) number giving initial value on legend e.g. "1.5"
        :param: colgrades_Num: (str) if Ints is "undefined" then this defines the number of legend incrememts e.g. "7", otherwise set to "undefined"
        :param: colgrades_userDefined: if Ints and Inis are both "undefined" this can be used to define the legend incrememts "[0,0.03,0.05,0.95,0.97,1]"
        :param: legend: (str) javaScript boolean ("true"/"false") defining whether or not the legend should be displayed on the DHA map, the colours and increments defined above with the prefix colgrades_ will be applied to the map whether or not a legend is displayed
        :param: sliderlabel: (str) string giving label for sliderbar e.g. "days in March"
        :param: mapStyles_weight: (str) defines the weight of the border surrounding each map feature e.g. "2"
        :param: mapStyles_opacity: (str) defines the opacity of the border surrounding each map feature, value range from 0 to 1, e.g. "0.8"
        :param: mapStyles_color: (str) defines the colour of the border surrounding each map feature using hexadecimal coding e.g. "#0000FF"
        :param: mapStyles_fillOpacity: (str) defines the opacity of the fill colour for each map feature, value range from 0 to 1, e.g. "0.8"
        :param: mapStyles_smoothFactor: (str) in relation to the map this defines a number which allows polyline simplification to improve performance times; the default is 1, higher numbers improve performance
        :param: mapStyles_radius: (str) if the map feature is a point this defines the radius of the point on the map e.g. "10"
        :param: noDataColour: (str) if the data is missing for a given feature on the map it displays as this colour e.g. "rgba(0, 0, 0, 0.3)"
        :param: featurehltStyle_weight: (str) defines the weight of the border surrounding each map feature e.g. "3" when it is selected on the map
        :param: featurehltStyle_color: (str) defines the colour of the border surrounding each map feature using hexadecimal coding e.g. "#0000FF"
        :param: timeData_xlabs: (str) x-axis labels on timeseries e.g. "['29 May', '05 Jun', '12 Jun', '19 Jun', '26 Jun', '03 Jul', '10 Jul', '17 Jul', '24 Jul']"
        :param: timeData_timeseries: (str) javaScript boolean, if "true" a timeseries will be displayed and timeData_xlabs must be defined
        :param: timeData_CIname: (str) name of confidence intervals if they exist, e.g. "0.05-0.95 quantiles"
        :param: timeData_highlight: (str) this is for an array defining the start ('xLeft') and finish ('xRight') of a band of background colour ('colorFill') along the timeseries x-axis (height of band is full length of y-axis) The 'title' parameter can be used to describe the band. If 'xLeft' is set to undefined the band will start at the lowest x-axis value. If 'xRight' is set to undefined the band will finish at the highest x-axis value. If a highlighted band is not required set 'highlight' to undefined.
        :param: timeData_timeseriesMin: (str) timeseries starting value as defined in geojsonname
        :param: timeData_timeseriesMax: (str) timeseries ending value as defined in geojsonname
        :param: timeData_timeseriesStep: (str) timeseries step size as defined in geojsonname
        :param: meandata: (str) this defines a javaScript list of data values which will be plotted on the timeseries and would typically be a country/region wide outcome (such as mean prevalance) e.g. "[ 3.16, 2.09, 1.27, 1.34, 1.14, 2.23, 5.05, 2.3, 1.71, 1.03, 1.06, 3.62 ]" these values are plotted in light blue with a filler (shaded area) down to zero on the y-axis
        :param: timePlot_Background1Colour: (str) defines the background colour of the 'meandata' (format hexadecimal or rgba)
        :param: timePlot_Line1Colour: (str) defines  the line colour of the 'meandata'(format hexadecimal or rgba)
        :param: timePlot_Background2Colour: (str) defines the background colour of the feature data (format hexadecimal or rgba)
        :param: timePlot_Line2Colour: (str) defines the line colour of the feature data (format hexadecimal or rgba)
        :param: timePlot_Background3Colour: (str) defines the background colour of the upper and lower confidence interval data (format hexadecimal or rgba)
        :param: timePlot_Line3Colour: (str) defines the line colour of the upper and lower confidence intervals data (format hexadecimal or rgba)
        :param: timePlot_MarkerSize: (str) defines the size of the markers on the timeseries e.g. "3"
        :param: timePlot_HighlightColour: (str) defines the line colour of the highlighted point and vertical line on timeseries (format hexadecimal or rgba)
        :param: timePlot_HighlightSize: (str) defines the marker size of the highlighted points on the timeseries plot e.g. "3"
        :param: timePlot_ymax: (str) defines the maximum y-axis value on the timeseries e.g. "10.5": note that if ymax="undefined" then the maximum y-axis value is computed dynamically
        :param: timePlot_beginYAtZero: (str) javaScript boolean, when "true" the timeseries y-axis starts at zero otherwise it is computed dynamically
        :param: layerMarker: (str) defines a dictionary of parameters used for putting a position marker in the map e.g. "{ latlng:[53.4875,-2.2901], popuptxt:'Salford UK' }"
        :param: mapBoundary: (str) defines a dictionary of parameters for placing an external boundary/border (polyline) around the map e.g. "{ filename:'data/externalBorder.geojson', borderStyle:{color:'#aaa', weight:2} }" where the geojson file is relative to the website
        :param: units_html: (str) javaScript string defining y-axis label for timeseries using html characters e.g. "cases / 10 <span style='vertical-align:super'>3</span> pop"
        :param: units_unicode: (str) javaScript string defining y-axis label for timeseries using unicode characters e.g. "cases / 10\u00B3 pop"
        :param: units_xlab: (str) javaScript string defining x-axis label for timeseries using html coding e.g. "week numbers in 2020"
        :param: downloadData: (str) string containing path (relative to website root) and file name of data being displayed on the map e.g. "data/Prevalence_per_100k.csv": this file can be any suitable format e.g. xlsx, geojson, ...
        :param: popupBox: (str) javaScript boolean defines whether a popup text box ("true") or inset box ("false") is used by the DHA to display details of feature/sub-region when the user clicks/taps on it
        :return: (str) javaScript dictionary of key:pair values for DHA
        """

        if sliderlabel == "undefined":
            sliderlabel = ""
        if meandata == "undefined":
            meandata = "[null]"
        if timeData_timeseries == "false":
            timePlot = "undefined"
        else:
            timePlot = self.timePlot(
                timePlot_Background1Colour,
                timePlot_Line1Colour,
                timePlot_Background2Colour,
                timePlot_Line2Colour,
                timePlot_Background3Colour,
                timePlot_Line3Colour,
                timePlot_MarkerSize,
                timePlot_HighlightColour,
                timePlot_HighlightSize,
                timePlot_ymax,
                timePlot_beginYAtZero,
            )

        layer = (
            "{ \n"
            "filename: " + self.toStr(geoJsonFile) + ",\n"
            "friendlyname: " + self.toStr(friendlyName) + ",\n"
            "radioButtonValue: " + self.toStr(radioButtonValue) + ",\n"
            "geojsonname: " + str(geojsonName) + ",\n"
            "layerName: " + str(layerName) + ",\n"
            "geojsongeom: " + self.toStr(geojsonGeom) + ",\n"
            "geojsonExtraInfo: " + self.toStr(geojsonExtraInfo) + ",\n"
            "mapPosition: "
            + self.mapPosition(mapPosition_centerLatLng, mapPosition_zoom)
            + ",\n"
            "regionNames: "
            + self.regionNames(regionNames_country, regionNames_area)
            + ",\n"
            "colgrades: "
            + self.colgrades(
                colgrades_colours,
                colgrades_legtitle,
                colgrades_Ints,
                colgrades_Inis,
                colgrades_Num,
                colgrades_userDefined,
            )
            + ",\n"
            "legend: " + legend + ",\n"
            "sliderlabel: " + self.toStr(sliderlabel) + ",\n"
            "mapStyles: "
            + self.mapStyles(
                mapStyles_weight,
                mapStyles_opacity,
                mapStyles_color,
                mapStyles_fillOpacity,
                mapStyles_smoothFactor,
                mapStyles_radius,
            )
            + ",\n"
            "noDataColour: " + self.toStr(noDataColour) + ",\n"
            "featurehltStyle: "
            + self.featurehltStyle(featurehltStyle_weight, featurehltStyle_color)
            + ",\n"
            "timeData: "
            + self.timeData(
                timeData_xlabs,
                timeData_timeseries,
                timeData_CIname,
                timeData_highlight,
                timeData_timeseriesMin,
                timeData_timeseriesMax,
                timeData_timeseriesStep,
            )
            + ",\n"
            "meandata: " + str(meandata) + ",\n"
            "timePlot: " + timePlot + ",\n"
            "subsetOfLayers:undefined" + ",\n"
            "layerMarker: " + layerMarker + ",\n"
            "mapBoundary: " + mapBoundary + ",\n"
            "units: " + self.units(units_html, units_unicode, units_xlab) + ",\n"
            "downloadData: " + self.toStr(downloadData) + ",\n"
            "popupBox: " + popupBox + "\n}"
        )
        return layer
