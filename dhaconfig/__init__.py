"""dynamic health atlas python config"""

from dhaconfig.config import (
    dha,
    utilities
)

__all__ = [
    "dha",
    "utilities"
]
